import Jasmine from "jasmine";
import {jam_it} from "./src";
const jasmine = new Jasmine();

jasmine.loadConfig({
    spec_files: [
        'src/**/*.spec.ts',
    ],
});
jasmine.exitOnCompletion = false;

jasmine.clearReporters();
jasmine.addReporter(jam_it);

jasmine.execute();