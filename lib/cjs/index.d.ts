/// <reference types="jasmine" />
import JasmineDoneInfo = jasmine.JasmineDoneInfo;
import JasmineStartedInfo = jasmine.JasmineStartedInfo;
import SpecResult = jasmine.SpecResult;
import SuiteResult = jasmine.SuiteResult;
interface Count {
    passed: number;
    failed: number;
    total: number;
}
declare class RastaReporter {
    graph: string;
    count: Count;
    overallCount: Count;
    jasmineStarted(_: JasmineStartedInfo): void;
    suiteStarted(result: SuiteResult): void;
    specStarted(_: SpecResult): void;
    specDone(result: SpecResult): void;
    suiteDone(_: SuiteResult): void;
    jasmineDone(result: JasmineDoneInfo): void;
}
export declare const jam_it: RastaReporter;
export {};
