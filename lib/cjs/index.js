"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jam_it = void 0;
const console_color_1 = require("@l.baste/console-color");
class RastaReporter {
    constructor() {
        this.graph = '';
        this.count = { passed: 0, failed: 0, total: 0 };
        this.overallCount = { passed: 0, failed: 0, total: 0 };
    }
    jasmineStarted(_) {
        console_color_1.cc.log('$<green>Rasta $<yellow>Jasmine $<red>Testing $<reset>!\n');
        this.overallCount = { passed: 0, failed: 0, total: 0 };
    }
    suiteStarted(result) {
        this.graph = '';
        this.count = { passed: 0, failed: 0, total: 0 };
        console_color_1.cc.log(`$<blue>[Feature]$<reset> ${result.description}`);
    }
    specStarted(_) {
        console.log = (...data) => {
            var _a, _b;
            const at = (_b = (_a = new Error().stack) === null || _a === void 0 ? void 0 : _a.split('\n')[2].trim()) !== null && _b !== void 0 ? _b : '';
            console_color_1.cc.log(`\n$<cyan>--- ${at} ---\n\n>  ${data}\n\n----${at.replace(/./g, '-')}----\n`);
        };
    }
    specDone(result) {
        console.log = console_color_1.cc.cLog;
        this.count[result.status]++;
        this.overallCount[result.status]++;
        this.count.total++;
        this.overallCount.total++;
        const tag = result.status === 'passed' ? '$<green>' : '$<red>';
        this.graph += `${tag}¤`;
        console_color_1.cc.log(`${tag}[${result.status}]$<reset> ${result.description}`);
        result.failedExpectations.forEach(failure => {
            console_color_1.cc.log('    ' + failure.message);
            console_color_1.cc.log('    ' + failure.stack.split('\n')[1] + '\n');
        });
    }
    suiteDone(_) {
        const tag = this.count.failed === 0 ? '$<green>' : '$<red>';
        console_color_1.cc.log(`${tag}${this.count.passed}$<reset>/$<yellow>${this.count.total}`);
        console_color_1.cc.log(this.graph, '\n');
    }
    jasmineDone(result) {
        const tag = result.overallStatus === 'passed' ? '$<green>' : '$<red>';
        console_color_1.cc.log('$<yellow>Done');
        console_color_1.cc.log(`Total time: ${result.totalTime / 1000} s`);
        console_color_1.cc.log(`Status: ${tag}${result.overallStatus}`);
        console_color_1.cc.log(`${tag}${this.overallCount.passed}$<reset>/$<yellow>${this.overallCount.total}`);
        console_color_1.cc.log(`$<red>${'¤'.repeat(this.overallCount.failed)}`);
    }
}
exports.jam_it = new RastaReporter();
