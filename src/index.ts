import {cc} from '@l.baste/console-color';
import JasmineDoneInfo = jasmine.JasmineDoneInfo;
import JasmineStartedInfo = jasmine.JasmineStartedInfo;
import SpecResult = jasmine.SpecResult;
import SuiteResult = jasmine.SuiteResult;

interface Count {
    passed: number,
    failed: number,
    total: number,
}

class RastaReporter {
    graph = '';
    count: Count = { passed: 0, failed: 0, total: 0 };
    overallCount: Count = { passed: 0, failed: 0, total: 0 };

    jasmineStarted(_: JasmineStartedInfo) {
        cc.log('$<green>Rasta $<yellow>Jasmine $<red>Testing $<reset>!\n');
        this.overallCount = { passed: 0, failed: 0, total: 0 };
    }
    suiteStarted(result: SuiteResult) {
        this.graph = '';
        this.count = { passed: 0, failed: 0, total: 0 };
        cc.log(`$<blue>[Feature]$<reset> ${result.description}`);
    }
    specStarted(_: SpecResult) {
        console.log = (...data) => {
            const at = new Error().stack?.split('\n')[2].trim() ?? '';
            cc.log(`\n$<cyan>--- ${at} ---\n\n>  ${data}\n\n----${at.replace(/./g, '-')}----\n`);
        };
    }
    specDone(result: SpecResult) {
        console.log = cc.cLog;
        this.count[<keyof Count>result.status]++;
        this.overallCount[<keyof Count>result.status]++;
        this.count.total++;
        this.overallCount.total++;
        const tag = result.status === 'passed' ? '$<green>' : '$<red>';
        this.graph += `${tag}¤`;
        cc.log(`${tag}[${result.status}]$<reset> ${result.description}`);
        result.failedExpectations.forEach(failure => {
            cc.log('    ' + failure.message);
            cc.log('    ' + failure.stack.split('\n')[1] + '\n');
        })
    }
    suiteDone(_: SuiteResult) {
        const tag = this.count.failed === 0 ? '$<green>' : '$<red>';
        cc.log(`${tag}${this.count.passed}$<reset>/$<yellow>${this.count.total}`);
        cc.log(this.graph, '\n');
    }
    jasmineDone(result: JasmineDoneInfo) {
        const tag = result.overallStatus === 'passed' ? '$<green>' : '$<red>';
        cc.log('$<yellow>Done');
        cc.log(`Total time: ${result.totalTime / 1000} s`);
        cc.log(`Status: ${tag}${result.overallStatus}`);
        cc.log(`${tag}${this.overallCount.passed}$<reset>/$<yellow>${this.overallCount.total}`);
        cc.log(`$<red>${'¤'.repeat(this.overallCount.failed)}`);
    }
}

export const jam_it = new RastaReporter();